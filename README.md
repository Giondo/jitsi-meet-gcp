# Installing and configuring Jitsi meet on gcp
## the automated way

## How to use it

You need to authenticated with Google Cloud in order to create several components

### Login with Google Cloud
```bash
gcloud auth application-default login
```

### CloudFlare API

You will need to get the Global api_key for CloudFlare
(it seems there is an issue reading the zones with the personal keys)

### Fill out all vars

Check and fill out all variables on vars.tf

## Requirements
* gcloud utils
* terraform >= 0.13
* cloudflare plugin ~> 2.0
