variable "jitsi_instance_type" {
	default = "e2-medium"
}
variable "IMAGE_NAME" {
	default = "debian-10-buster-v20200910"
}
variable "PROJECT_NAME" {
	default = "playground-s-11-b9d91b31"
}
variable "LETSENCRYPT_EMAIL" {
	default = "email@email.com"
}
variable "CLOUDFLARE_EMAIL" {
	default = "email@email.com"
}
variable "CLOUDFLARE_TOKEN" {
	default = "token"
}
variable "DOMAIN" {
	default = "virtualinfra.online"
}
variable "PUBLIC_HOST" {
	default = "meet"
}
