output "vmname" {
	value = google_compute_instance.jitsivm.name
}
output "ExternalIP" {
	value = google_compute_address.static_ext_ip.address
}
output "InternalIP" {
	value = google_compute_instance.jitsivm.network_interface[0].network_ip
}
output "CONNECT" {
	value = "ssh ${google_compute_address.static_ext_ip.address} -L 8888:localhost:32400"
}
output "PUBLIC_URL" {
	value = "https://${var.PUBLIC_HOST}.${var.DOMAIN}"
}
