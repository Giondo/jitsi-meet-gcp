resource "google_compute_instance" "jitsivm" {
    name         = "jitsi-server"
    machine_type = var.jitsi_instance_type
    tags = ["jitsi"]
    #allow_stopping_for_update = true

    boot_disk {
      initialize_params {
        image = var.IMAGE_NAME
        size  = 50
      }
    }

    network_interface {
      network = google_compute_network.jitsi-network.name
      access_config {
            nat_ip = google_compute_address.static_ext_ip.address
        }
      }



    metadata_startup_script = file("scripts/init_script-jitsi.sh")

}
