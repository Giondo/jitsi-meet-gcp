#!/bin/bash

#GCSFUSE to mount the GCS Bucket where the movies are
#Add GCSFuse REPO
#https://cloud.google.com/storage/docs/gcs-fuse
apt update -y
apt install apt-transport-https git docker-compose -y

# curl https://download.jitsi.org/jitsi-key.gpg.key | sudo sh -c 'gpg --dearmor > /usr/share/keyrings/jitsi-keyring.gpg'
# echo 'deb [signed-by=/usr/share/keyrings/jitsi-keyring.gpg] https://download.jitsi.org stable/' | sudo tee /etc/apt/sources.list.d/jitsi-stable.list > /dev/null

# update all package sources
# apt update -y
#apt install jitsi-meet -y

git clone https://github.com/jitsi/docker-jitsi-meet /root/docker-jitsi-meet && cd /root/docker-jitsi-meet
# cp /root/docker-jitsi-meet/env.example /root/docker-jitsi-meet/.env
cp /tmp/envvar.env /root/docker-jitsi-meet/.env
cd /root/docker-jitsi-meet && ./gen-passwords.sh
mkdir -p ~/.jitsi-meet-cfg/{web/letsencrypt,transcripts,prosody/config,prosody/prosody-plugins-custom,jicofo,jvb,jigasi,jibri}
cd /root/docker-jitsi-meet && docker-compose up -d
