resource "google_compute_firewall" "default" {
  name    = "firewall-jitsi"
  network = google_compute_network.jitsi-network.name

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["22", "80", "443", "4433"]
  }

  allow {
    protocol = "udp"
    ports    = ["10000"]
  }


  target_tags = ["jitsi"]
  source_ranges = ["0.0.0.0/0"]
}
