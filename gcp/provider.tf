provider "google" {
  # credentials = "${file("account.json")}"
  project     = var.PROJECT_NAME
  region      = "europe-west4"
  zone        = "europe-west4-c"
}

provider "cloudflare" {
  version = "~> 2.0"
  email = var.CLOUDFLARE_EMAIL
  api_key = var.CLOUDFLARE_TOKEN
}
