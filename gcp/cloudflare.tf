data "cloudflare_zones" "domain" {
  filter {
    name = var.DOMAIN
  }
}
# Add a record to the domain
resource "cloudflare_record" "PUBLIC_HOST" {
  zone_id = data.cloudflare_zones.domain.zones[0].id
  name    = var.PUBLIC_HOST
  value   = google_compute_address.static_ext_ip.address
  type    = "A"
  # ttl     = 3600
}
