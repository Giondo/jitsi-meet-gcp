data "template_file" "envvar" {
  template = file("template/env_var.tpl")

  vars = {
    LETSENCRYPT_EMAIL = var.LETSENCRYPT_EMAIL
    PUBLIC_URL = "${var.PUBLIC_HOST}.${var.DOMAIN}"
    LOCAL_IP = google_compute_instance.jitsivm.network_interface[0].network_ip
  }
}

# output "policy" {
#   value = "${template_file.policy.rendered}"
# }

resource "null_resource" "pretend_gcp_iam_policy" {
  triggers = {
    envvar = data.template_file.envvar.rendered
  }


    provisioner "file" {
      content     = data.template_file.envvar.rendered
      destination = "/tmp/envvar.env"
      connection {
        host = google_compute_address.static_ext_ip.address
        type = "ssh"
        user = "giondo"
        private_key = file("~/.ssh/id_rsa")
        }
    }
}
